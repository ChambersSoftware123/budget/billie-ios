//
//  BudgetData.swift
//  billie2
//
//  Created by Thomas Chambers on 6/22/23.
//

import XCTest
import Foundation
@testable import billie2

final class BudgetDataTests: XCTestCase {
    
    var budgetData: BudgetData!

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        super.setUp()
        budgetData = BudgetData()
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        budgetData = nil
        super.tearDown()
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        // Any test you write for XCTest can be annotated as throws and async.
        // Mark your test throws to produce an unexpected failure when your test encounters an uncaught error.
        // Mark your test async to allow awaiting for asynchronous code to complete. Check the results with assertions afterwards.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testUpdateBudget() {
        let calendar = Calendar.current
        let currentDate = Date()
        var dateComponents = calendar.dateComponents([.year, .month, .day], from: currentDate)
        dateComponents.day! -= 1
        let incomeDate = calendar.date(from: dateComponents)
    
        budgetData.income = 1000.0
        budgetData.dailyBudget = 10.0
        budgetData.remainingBudget = 100.0
        budgetData.incomeDate = incomeDate

        
        // Call the method being tested
        budgetData.updateBudget()
        
        // Assert the expected results
        XCTAssertEqual(budgetData.remainingBudget, 110.0)
    }

}
