//
//  billie2App.swift
//  billie2
//
//  Created by Thomas Chambers on 6/20/23.
//

import SwiftUI

@main
struct Billie: App {
    @StateObject private var budgetData = BudgetData()
    @Environment(\.scenePhase) private var scenePhase
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environmentObject(budgetData)
        }
        .onChange(of: scenePhase) { phase in
            switch phase {
            case .active:
                budgetData.updateBudget()
            default:
                break
            }
            budgetData.handleBackgroundTransition(to: phase)
        }
    }
}
