//
//  MainBudgetView.swift
//  billie2
//
//  Created by Thomas Chambers on 6/20/23.
//

import Foundation
import SwiftUI

struct MainBudgetView: View {
    @EnvironmentObject var budgetData: BudgetData
    @State private var spendingInput: String = ""
    
    var body: some View {
        VStack {
            RoundRectTextInputView(text: "Enter Spending", spendingInput: $spendingInput, budgetData: budgetData)
            RoundRectTextView(text: "Remaining Budget", integer: Int(budgetData.remainingBudget))
            RoundRectTextView(text: "Daily Budget", integer: Int(budgetData.dailyBudget))
        }
    }
}



