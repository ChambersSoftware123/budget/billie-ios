//
//  ContentView.swift
//  billie2
//
//  Created by Thomas Chambers on 6/20/23.
//

import SwiftUI

struct ContentView: View {
    @EnvironmentObject var budgetData: BudgetData
    
    var body: some View {
        ScrollView {
            VStack {
                Text("BILLIE")
                    .bold()
                    .lineSpacing(4)
                    .font(.largeTitle)
                    .kerning(2)
                    .foregroundColor(.black)
                    .padding(20)
                Group {
                    if budgetData.dailyBudget > 0 {
                        MainBudgetView()
                    } else {
                        IncomeInputView()
                    }
                }
            }
        }
        .frame(maxWidth: .infinity)
    }
    
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
            .environmentObject(BudgetData())
    }
}
