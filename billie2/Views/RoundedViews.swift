//
//  RoundedViews.swift
//  Billie
//
//  Created by Thomas Chambers on 3/24/23.
//

import SwiftUI

struct RoundedImageViewStroked: View {
    var systemName: String
    
    var body: some View {
        Image(systemName: systemName)
            .font(.title)
            .foregroundColor(.black)
            .frame(width: 56, height: 56)
            .overlay(
                Circle()
                    .strokeBorder(.gray, lineWidth: 2)
            )
    }
}

struct RoundedImageViewFilled: View {
    var systemName: String
    
    var body: some View {
        Image(systemName: systemName)
            .font(.title)
            .foregroundColor(.black)
            .frame(width: 56, height: 56)
            .background(
                Circle()
                    .fill(.white)
            )
            .overlay(
                Circle()
                    .strokeBorder(.gray, lineWidth: 2)
            )
    }
}

struct RoundRectTextView: View {
    var text: String
    var integer: Int
    
    var body: some View {
        Text("\(text)")
            .multilineTextAlignment(.center)
            .font(.title3)
            .padding(.top)
        Text("\(integer)")
            .multilineTextAlignment(.center)
            .frame(width: Constants.General.roundRectViewWidth, height: Constants.General.roundRectViewHeight)
            .overlay(
                RoundedRectangle(cornerRadius: Constants.General.roundRectCornerRadius).strokeBorder(Color("TextColor"), lineWidth: Constants.General.strokeWidth))
            .foregroundColor(Color("TextColor"))
            .kerning(1)
            .cornerRadius(Constants.General.roundRectCornerRadius)
            .fontWeight(.black)
            .font(.largeTitle)
        Spacer()
    }
}

struct RoundRectTextInputView: View {
    var text: String
    @Binding var spendingInput: String
    @State private var originalSpendingInput: String
    var budgetData: BudgetData
    
    init(text: String, spendingInput: Binding<String>, budgetData: BudgetData) {
        self.text = text
        self._spendingInput = spendingInput
        self._originalSpendingInput = State(initialValue: spendingInput.wrappedValue)
        self.budgetData = budgetData
    }
    
    var body: some View {
            Text("\(text)")
                .multilineTextAlignment(.center)
                .font(.title3)
                .padding(.top)
            TextField("Enter Spending", text: $spendingInput)
                .keyboardType(.numberPad)
                .multilineTextAlignment(.center)
                .frame(width: Constants.General.roundRectViewWidth, height: Constants.General.roundRectViewHeight)
                .overlay(
                    RoundedRectangle(cornerRadius: Constants.General.roundRectCornerRadius).strokeBorder(Color("TextColor"), lineWidth: Constants.General.strokeWidth))
                .foregroundColor(Color("TextColor"))
                .kerning(1)
                .cornerRadius(Constants.General.roundRectCornerRadius)
                .font(.body)
            Spacer()
            
            Button(action: {
                if let spending = Double(spendingInput) {
                    budgetData.subtractSpending(amount: spending)
                    spendingInput = originalSpendingInput // Reset to original state
                }
            }, label: {
                Text("Submit Spending".uppercased())
                    .padding(20.0)
                    .background(
                        ZStack {
                            Color("ButtonColor")
                            LinearGradient(colors: [Color.white.opacity(0.3), Color.clear], startPoint: .top, endPoint: .bottom)
                        }
                    )
                    .overlay(
                        RoundedRectangle(cornerRadius: Constants.General.roundRectCornerRadius).strokeBorder(Color.white, lineWidth: Constants.General.strokeWidth))
                    .foregroundColor(.white)
                    .cornerRadius(Constants.General.roundRectCornerRadius)
                    .bold()
                    .font(.title3)
            })
        }
    }

struct PreviewView: View {
    var body: some View {
        VStack {
            RoundRectTextView(text: "Daily Budget", integer: 10)
            RoundedImageViewFilled(systemName: "arrow.counterclockwise")
            RoundedImageViewStroked(systemName: "list.dash")
        }
    }
}



struct RoundedViews_Previews: PreviewProvider {
    static var previews: some View {
        PreviewView()
        PreviewView()
            .preferredColorScheme(.dark)
    }
}
