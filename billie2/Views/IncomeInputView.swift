//
//  IncomeInputView.swift
//  billie2
//
//  Created by Thomas Chambers on 6/20/23.
//

import Foundation
import SwiftUI

struct IncomeInputView: View {
    @EnvironmentObject var budgetData: BudgetData
    @State private var incomeInput: String = ""
    
    var body: some View {
        VStack {
            Text("Please enter your income")
            TextField("Enter Income", text: $incomeInput)
                .keyboardType(.numberPad)
                .textFieldStyle(RoundedBorderTextFieldStyle())
                .padding()
            Button(action: {
                if let income = Double(incomeInput) {
                    budgetData.income = income
                    budgetData.calculateDailyBudget()
                }
            }, label: {
                Text("Enter")
            })
            .padding(20.0)
            .background(
                ZStack {
                    Color("ButtonColor")
                    LinearGradient(colors: [Color.white.opacity(0.3), Color.clear], startPoint: .top, endPoint: .bottom)
                }
            )
            .overlay(
                RoundedRectangle(cornerRadius: Constants.General.roundRectCornerRadius).strokeBorder(Color.white, lineWidth: Constants.General.strokeWidth))
            .foregroundColor(.white)
            .cornerRadius(Constants.General.roundRectCornerRadius)
            .bold()
            .font(.title3)
        }
    }
}

struct IncomeInputView_Previews: PreviewProvider {
    static var previews: some View {
        IncomeInputView()
            .environmentObject(BudgetData())
    }
}
