import Foundation
import SwiftUI
import Combine

class BudgetData: ObservableObject {
    @Published var income: Double = 0.0 {
        didSet {
            UserDefaults.standard.set(income, forKey: "Income")
        }
    }
    
    @Published var dailyBudget: Double = 0.0 {
        didSet {
            UserDefaults.standard.set(dailyBudget, forKey: "DailyBudget")
        }
    }
    @Published var remainingBudget: Double = 0.0 {
        didSet {
            UserDefaults.standard.set(remainingBudget, forKey: "RemainingBudget")
        }
    }
    @Published var spending: Double = 0.0
    var incomeDate: Date? {
        didSet {
            if let incomeDate = incomeDate {
                UserDefaults.standard.set(incomeDate, forKey: "IncomeDate")
            }
        }
    }
    
    init() {
        self.income = UserDefaults.standard.double(forKey: "Income")
        self.dailyBudget = UserDefaults.standard.double(forKey: "DailyBudget")
        self.remainingBudget = UserDefaults.standard.double(forKey: "RemainingBudget")
        self.incomeDate = UserDefaults.standard.object(forKey: "IncomeDate") as? Date
        updateBudget()
    }

    func calculateDailyBudget() {
        dailyBudget = (income * 0.3) / 30
        remainingBudget += dailyBudget
    }
    
    func subtractSpending(amount: Double) {
        remainingBudget -= amount
    }
    
    func updateBudget() {
        let currentDate = Date()
        if let incomeDate = self.incomeDate {
            let calendar = Calendar.current
            let components = calendar.dateComponents([.day], from: incomeDate, to: currentDate)
            let daysPassed = components.day ?? 0
            
            remainingBudget += Double(daysPassed) * (dailyBudget)
        }
        
        self.incomeDate = currentDate
    }
    
    func handleBackgroundTransition(to phase: ScenePhase) {
        if phase == .background {
            updateBudget()
        }
    }
}
