//
//  Constants.swift
//  Billie
//
//  Created by Thomas Chambers on 3/31/23.
//

import Foundation

enum Constants {
    enum General {
        public static let strokeWidth = CGFloat(2.0)
        public static let roundViewLength = CGFloat(56)
        public static let roundRectViewWidth = CGFloat(160)
        public static let roundRectViewHeight = CGFloat(130)
        public static let roundRectCornerRadius = CGFloat(21)
    }
    
    enum SpendingHistory {
        public static let scoreColumnWidth = CGFloat(50)
        public static let dateColumnWidth = CGFloat(170)
        public static let maxRowWidth = CGFloat(480)
    }
}
